/*~~~~~~~~~~~~~~~~~~~~~~~~~Sarah Wadley~~~~~~~~~~~~~~~~~~~~~~~~~*/
/*~~~~~~~~~~~~~~~~~~~~~COP4531 - Algorithms~~~~~~~~~~~~~~~~~~~~~*/
/*~~~~~~~~~~~~~~~~~11/16 - Programming Project 2~~~~~~~~~~~~~~~~~*/

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include <time.h>

//output contents to a txt file called output.txt
void writeFile(std::vector<std::vector<int>> &rows, std::string filename="outhere.txt")
{
  std::ofstream myfile(filename);

  for(int i = 0; i < rows.size(); i++)
  {
    for(int j = 0; j < rows[i].size(); j++)
    {
      myfile << rows[i][j];
      if(j+1 < rows[i].size())
      {
        myfile << ',';
      }
      else{
        myfile << '\n';
      }
    }
  }
  myfile.close();
}

//print first 10x10 pixel sequence
void printPortion(std::vector<std::vector<int>> const &rows)
{
  //std::vector<std::vector<int>> results = rows;
  for(int i = 0; i < 10; i++)
  {
    for(int j = 0; j < 10; j++)
    {
        std::cout << rows[i][j];

        if(j+1 >= 10)
        {
          std::cout << '\n';
        }
        else{
          std::cout << '\t';
        }
    }
  }
  std::cout << '\n';
}

//vector from no input file
void makeData(std::vector<std::vector<int>> &rows)
{
    //rows hold 256 'pixels' vectors, which hold 256 ints each
    std::vector<int> pixels;
    int a,b=0;
    while(b< 256)
    {  if(a>=256) //we assume input will be 256x256
      {
          //cap the row and record it twice
          //ensures pixels scale vertically
          rows.push_back(pixels);
          rows.push_back(pixels);

          //reset i and vector so we can refill
          pixels.clear();
          a=0;
          b++;
      }
      //record each value as an integer twice
      //ensures pixels scale horizontally
      pixels.push_back(69);
      pixels.push_back(69);

      a++;
    }


}

//return vector from parsed file, nested to preserve 256x256 pixel positions
std::vector<std::vector<int>> fillVector(std::string filename)
{
    //rows hold 256 'pixels' vectors, which hold 256 ints each
    std::vector<std::vector<int>> rows;
    std::vector<int> pixels;

    //arbitrary variables for parse loop
    std::string line, elmt;
    int rowSize, i = 0;


    //open the txt file
    std::ifstream file(filename);
    if(file.is_open())
      {   //read a line and plug it into stringstream
          while(getline(file, line))
          {
              std::stringstream ss(line);
              //values are comma separated, so delimit the row by ','
              while(getline(ss,elmt,','))
              {
                  if(i>=256) //we assume input will be 256x256
                  {
                      //cap the row and record it twice
                      //ensures pixels scale vertically
                      rows.push_back(pixels);
                      rows.push_back(pixels);


                      //reset i and vector so we can refill
                      pixels.clear();
                      i=0;
                  }
                  //record each value as an integer twice
                  //ensures pixels scale horizontally
                  pixels.push_back(std::stoi(elmt));
                  pixels.push_back(std::stoi(elmt));

                  i++;
              }
          }
          //loop ends before adding last 2 rows
          rows.push_back(pixels);
          rows.push_back(pixels);

   }
   //close the txt file
   file.close();

      return rows;
}

//read in file or create one, produce 512x512 image.txt
int main(int argc, char* argv[])
{
  //time our algorithm
  clock_t start = clock();

  //a vector of vectors will hold our pixel ints in rows of 512
  std::vector<int> pixels;
  std::vector<std::vector<int>> rows;

  if(argv[1]!=NULL)
  {
      std::string file = argv[1];
      rows = fillVector(file);
  }
  else
  {
      makeData(rows);
  }


  if(argv[2]!=NULL)
  {
      std::string filey = argv[2];

      writeFile(rows,filey);
  }
  else
  {
      writeFile(rows);
  }

  std::cout << "pixel preview" << std::endl;
  printPortion(rows);

  //display execution time
  printf("Execution Time: %.2fs\n", (double)(clock()-start)/CLOCKS_PER_SEC);

      return 0;
}
