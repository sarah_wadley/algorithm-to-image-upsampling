main: usample.cpp
	g++ usample.cpp -std=c++11 -o main


test:
	g++ -std=c++11 -I /path/googletest/googletest/include/ -L /path/googletest/lib usample.cpp -lgtest -lpthread


coverage: usample.cpp
	g++ -std=c++11 -fprofile-arcs -ftest-coverage -fPIC usample.cpp
	./a.out
	~/.local/bin/gcovr -r . --html > coverage.html

clean:
	rm main
	rm output.txt
	rm a.out
	rm *.gcno
	rm *.gcda	

