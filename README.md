# Algorithm to Image Upsampling
Sarah Wadley
COP4531 ~ progHW2

our main program, usample.cpp, will take a txt file representing a 256 x 256 image with integer values. 
we must upsample factor of 2 and output to a file of the same type.

Project repo:
https://gitlab.com/sarah_wadley/algorithm-to-image-upsampling

to compile this program use the command 'make'
from the same directory as the makefile and usample.cpp
"./main" 			will run a demo without input needed
"./main example.txt" 		will run using example.txt as input
"./main in.txt out.txt" 	will run, prioducing output to out.txt

cpp file is documented and readable
displays time in seconds at termination


usample.cpp contains the following functions:

//output contents to a txt file called output.txt
void writeFile(std::vector<std::vector<int>> &rows, std::string filename="outhere.txt")

//print first 10x10 pixel sequence
void printPortion(std::vector<std::vector<int>> const &rows)

//vector from no input file
void makeData(std::vector<std::vector<int>> &rows)

//return vector from parsed file, nested to preserve pixel positions
std::vector<std::vector<int>> fillVector(std::string filename)



code not added to program:

//interpolation attempt
int interpolate(int i, int z, std::vector<std::vector<int>> &rows)
{
  int lefttop = rows[i][z];
  int righttop = rows[i][z+2];
  int leftbottom = rows[i+2][z];
  int rightbottom = rows[i+2][z+2];
  float x = 0.25;
  float y = 0.25;
  float  top1 = lefttop*(1 - x) + rightbottom*x;
  float  top2 = righttop*(1 - x) + lefttop*x;
  float  bottom1 = leftbottom*(1 - x) + rightbottom*x;
  float  bottom2 = rightbottom*(1 - x) + leftbottom*x;
  int left1 = (int)round(top1*(1 - y) + bottom1*y);
  int left2 = (int)round(bottom1*(1 - y) + top1*y);
  int right1 = (int)round(top2*(1 - y) + bottom2*y);
  int right2 = (int)round(bottom2*(1 - y) + top2*y);

  return left1, right1, left2, right2;

}
//assign values based on interpolation
void populate(std::vector<std::vector<int>> &rows)
{
  for(int i = 0; i < 4; i+=2)//<256
  {
    std::cout << "switching rows" << std::endl;
    for(int z=0; z<10;z+=2)//this runs a loop for newPixels1.size()-3
    {
      rows[i][z], rows[i][z+1], rows[i+1][z], rows[i+1][z+1] = interpolate(i,z,rows);
    }
  }
}

//self testing
void findDiff(std::vector<std::vector<int>> const &r1, std::vector<std::vector<int>> const &r2)
{
  double dsum = 0;
  int pixels = r1.size()*r1.size();
  double totsum = 0;

  for(int i = 0; i < r1.size(); i++)
  {
    for(int j = 0; j < r1[i].size(); j++)
    {
        dsum += (r2[i][j]-r1[i][j]);
        totsum += r2[i][j];
    }
  }
  //should be 512x512
  std::cout << "total positions: \t" << pixels << std::endl;

  std::cout << std::endl;
  std::cout << "tot diff between sums: \t" << dsum << std::endl;
  std::cout << "tot sum: \t\t" << totsum << std::endl;
  std::cout << std::endl;


  std::cout << "avg sum per pixel: \t" << totsum/pixels << std::endl;
  std::cout << "avg diff per pixel: \t" << dsum/pixels << std::endl;
  std::cout << std::endl;

  printf("percent different: \t%.3f %\n", ((double)dsum/(double)totsum)*100);
  std::cout << "\n" << std::endl;
}

